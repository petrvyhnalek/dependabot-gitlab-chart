#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

update_dependencies

log_with_header "Validate chart template files"
for yaml in .gitlab/ci/kube/values/*.yaml; do
  log_with_header "Validating $CHART_DIR with $yaml" "-"

  helm template $CHART_DIR -f $yaml | kubeval \
    --strict \
    --schema-location https://raw.githubusercontent.com/yannh/kubernetes-json-schema/master \
    --additional-schema-locations https://raw.githubusercontent.com/joshuaspence/kubernetes-json-schema/master
done
