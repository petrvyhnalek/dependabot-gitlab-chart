#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

log_with_header "Linting README.md"
helm-docs -c charts -o ../../README.md
git diff --exit-code
