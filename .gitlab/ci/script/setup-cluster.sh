#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

log_with_header "Setup cluster"

log "Create '$NAMESPACE' namespace"
kubectl create namespace "$NAMESPACE"

log "Create serviceMonitor CRD"
kubectl create -f https://raw.githubusercontent.com/prometheus-operator/kube-prometheus/main/manifests/setup/0servicemonitorCustomResourceDefinition.yaml

log "Install gitlab mock"
echo "** Adding repo **"
helm repo add andrcuns https://andrcuns.github.io/charts

echo "** Installing smocker chart **"
helm install gitlab andrcuns/smocker \
  -n gitlab \
  -f .gitlab/ci/config/mock-definitions.yaml \
  --create-namespace \
  --timeout 2m \
  --atomic

if [[ "$VALUES" == "secrets" ]]; then
  log "Create custom secrets for credentials"
  kubectl create -f .gitlab/ci/kube/secrets.yaml -n "$NAMESPACE"
elif [[ "$VALUES" == "ingress" ]]; then
  log "Setup ingress"
  kubectl apply -f https://projectcontour.io/quickstart/contour.yaml
  kubectl patch daemonsets -n projectcontour envoy -p '{"spec":{"template":{"spec":{"nodeSelector":{"ingress-ready":"true"},"tolerations":[{"key":"node-role.kubernetes.io/control-plane","operator":"Equal","effect":"NoSchedule"},{"key":"node-role.kubernetes.io/master","operator":"Equal","effect":"NoSchedule"}]}}}}'
fi
